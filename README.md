# **CS481_HW2**

[CSUSM](https://www.csusm.edu/) - Introduction in Mobile Programming.
Calculator App in Xamarin. (Rotation in landscape is disable)


![Screenshot from Kalkulator](screenshot.jpg)
## Feature

 - **Operations available:** 
	 - Addition
	 - Substraction
	 - Multiplication
	 - Division
	 - Modulo
 - **Types:**
	 - Decimal
	 - Negative number
 - **Error handling:**
	 - If you don't finish with a number **"2 + 3 +"**
	 - If you try to add a point "." and you are already in a decimal number **"2.3.4"**
	 - If you try to add a minus in a negative number **"- - -1"**
	 - If you launch or reset the app "AC" you can only start with minus or a number **"% 3"**
 - **Cool features:**
	 - If you have a result you can press an operator to start a new calculation with your result
	 - You can combine multiple operators together **"2 + 3.2 / 4 % 2"**
	 - Gradient "Look perfect on real devices and bad on Emulators"

doc.v1.0