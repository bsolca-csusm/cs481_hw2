﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kakulator.Service
{
    enum Operator
    {
        Addition,
        Subtraction,
        Multiplication,
        Division,
        Modulo
    }

    class Calculator
    {
        private double _result = 0;
        Stack<double> memoNumber = new Stack<double>();
        Stack<Operator> memoOperator = new Stack<Operator>();

        public void AddNumber(double number)
        {
            Console.WriteLine("AddNumber: " + number + "\n");
            memoNumber.Push(number);
        }

        public void AddOperator(Operator op)
        {
            Console.WriteLine("ADD_OPERATOR");
            memoOperator.Push(op);
        }

        public double Calculate()
        {
            Stack<double> numberStack = new Stack<double>();
            Stack<Operator> operatorsStack = new Stack<Operator>();
            while (memoNumber.Count > 0)
            {
                numberStack.Push(memoNumber.Pop());
            }
            while (memoOperator.Count > 0)
            {
                operatorsStack.Push(memoOperator.Pop());
            }

            _result = numberStack.Pop();
            while (numberStack.Count > 0)
            {
                Console.WriteLine("Calculate: " + _result + "\n");
                switch (operatorsStack.Pop())
                {
                    case Operator.Addition:
                        _result += numberStack.Pop();
                        break;
                    case Operator.Subtraction:
                        _result -= numberStack.Pop();
                        break;
                    case Operator.Multiplication:
                        _result *= numberStack.Pop();
                        break;
                    case Operator.Division:
                        _result /= numberStack.Pop();
                        break;
                    case Operator.Modulo:
                        _result %= numberStack.Pop();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            numberStack.Clear();
            operatorsStack.Clear();
            return _result;
        }

        public void Reset()
        {
            memoNumber.Clear();
            memoOperator.Clear();
            _result = 0;
        }
    }
}
