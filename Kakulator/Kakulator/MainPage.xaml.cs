﻿using System;
using System.ComponentModel;
using Kakulator.Service;
using Xamarin.Forms;

namespace Kakulator
{
    enum CalcState
    {
        Start,
        NegativeNumber,
        NegativeDouble,
        PositiveNumber,
        PositiveDouble,
        Operator,
        Result,
    }
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private readonly Calculator _calculator  = new Calculator();
        private string _memNumber = "";
        private CalcState _state = CalcState.Start;

        public MainPage()
        {
            InitializeComponent();
        }

        private void OnClickedNumber(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            string numberPressed = button.Text;

            if (numberPressed == "." && (_state == CalcState.NegativeDouble || _state == CalcState.PositiveDouble)) 
            {
                DisplayAlert("Information", "You already have a decimal number", "Got it");
                return;
            }
            
            if (this.Result.Text != "")
            {
                this.Calculation.Text = "";
                this.Result.Text = "";
            }

            if (numberPressed == "-")
            {
                _state = CalcState.NegativeNumber;
            } else if (numberPressed == ".")
            {
                _state = _state == CalcState.NegativeNumber ? CalcState.NegativeDouble : CalcState.PositiveDouble;
            } else
            {
                _state = _state == CalcState.PositiveDouble ? CalcState.PositiveDouble : CalcState.PositiveNumber;
            }

            _memNumber += numberPressed;
            this.Calculation.Text += numberPressed;
        }

        private void OnClickedReset(object sender, EventArgs e)
        {
            _state = CalcState.Start;
            this.Calculation.Text = "";
            this.Result.Text = "";
            _calculator.Reset();
        }

        private void OnClickedOperator(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            string operatorPressed = button.Text;
            if (this.Calculation.Text == "" && operatorPressed != "-")
            {
                DisplayAlert("Information", "Start with minus or a number", "Got it");
                return;
            }
            if ((_state == CalcState.Operator || _state == CalcState.Start) && operatorPressed == "-")
            {
                this.OnClickedNumber(sender, e);
                return;
            }
            if ((operatorPressed == "-" || operatorPressed == "+") && (_state == CalcState.NegativeDouble || _state == CalcState.NegativeNumber))
            {
                DisplayAlert("Information", "You already have a negative number", "Got it");
                return;
            }
            if (_state == CalcState.Operator || this.Calculation.Text.EndsWith("-"))
            {
                DisplayAlert("Information", "You already have an operator", "Got it");
                return;
            }

            _state = CalcState.Operator;
            if (this.Result.Text != "")
            {
                this.Calculation.Text = this.Result.Text;
                _calculator.AddNumber(Convert.ToDouble(this.Result.Text));
                this.Result.Text = "";
            }
            else
            {
                _calculator.AddNumber(Convert.ToDouble(_memNumber));
            }
            _memNumber = "";
            
            this.Calculation.Text += " " + operatorPressed + " ";
            
            switch (operatorPressed)
            {
               case "+":
                    _calculator.AddOperator(Operator.Addition);
                   break;
               case "-":
                   _calculator.AddOperator(Operator.Subtraction);
                   break;
               case "*":
                   _calculator.AddOperator(Operator.Multiplication);
                   break;
               case "/":
                   _calculator.AddOperator(Operator.Division);
                   break;
               case "%":
                   _calculator.AddOperator(Operator.Modulo);
                   break;
               default:
                   throw new NotImplementedException();
            }

            _state = CalcState.Operator;
        }
        private void OnClickedCalculate(object sender, EventArgs e)
        {
            if (_state == CalcState.Result)
                return;
            if (_state == CalcState.Operator || _state == CalcState.Start || _memNumber == "+" || _memNumber == "-")
            {
                DisplayAlert("Information", "You must finish with a number", "Got it");
                return; 
            }
            if (_memNumber.EndsWith("."))
            {
                DisplayAlert("Information", "Don't finish with a period", "Got it");
                return; 
            }
            _calculator.AddNumber(Convert.ToDouble(_memNumber));
            double res = _calculator.Calculate();
            this.Result.Text = res.ToString();
            _memNumber = "";
            _calculator.Reset();
            _state = CalcState.Result;
        }
    }
}
